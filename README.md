Az alkalmazás docker alól fut, így ez szükséges hozzá.

~~~~
Le kell húzni a repót, minden változás a master-be benne van.
~~~~

`git checkout master`

Majd futtatni kell a docker container-t.

`docker-compose up -d`

Ezután be kell lépni a container-be, futtatni a composer update-et, és a migrációkat.

Linux alatt:

`docker exec -it loginhu_test_php_1 bash`

Windows alatt:

`winpty docker exec -it loginhu_test_php_1 bash`

A container alatt pedig ezeket:

`composer update`

`yii migrate`

`itt a yes-re kell menni`

Ezek után böngészőből el lehet érni az alkalmazást már.

http://localhost:8000/employees/index

PHPMyAdmin

user/pass: loginhu/Abc123

http://localhost:9191/

Bármi kérdés van: info@szabolcs-toth.com