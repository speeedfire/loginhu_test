<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\domains\as\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Employees', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'emp_no',
            'birth_date',
            'first_name',
            'last_name',
            'gender',
            'hire_date',
            [
                'class' => DataColumn::className(),
                'attribute'=>'currentSalaries.salary',
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'dept',
                'value'=>'currentDeptNos.dept_name',
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'title',
                'value'=>'currentTitles.title',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
