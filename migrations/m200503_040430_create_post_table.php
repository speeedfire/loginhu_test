<?php
use yii\db\Migration;

class m200503_040430_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createIndex(
            'idx-salaries-dates',
            'salaries',
            [
                'from_date',
                'to_date',
            ]
        );

        $this->createIndex(
            'idx-titles-dates',
            'titles',
            [
                'from_date',
                'to_date',
            ]
        );

        $this->createIndex(
            'idx-dept_emp-dates',
            'dept_emp',
            [
                'from_date',
                'to_date',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropIndex(
            'idx-salaries-dates',
            'salaries'
        );

        $this->dropIndex(
            'idx-titles-dates',
            'titles'
        );

        $this->dropIndex(
            'idx-dept_emp-dates',
            'dept_emp'
        );
    }
}