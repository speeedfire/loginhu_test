<?php

namespace app\domains\asearch;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\domains\ar\Employees;

/**
 * EmployeesSearch represents the model behind the search form of `app\domains\ar\Employees`.
 */
class EmployeesSearch extends Employees
{
    public $title;
    public $dept;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_no'], 'integer'],
            [['birth_date', 'first_name', 'last_name', 'gender', 'hire_date', 'title', 'dept'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();
        $query->joinWith(['currentTitles', 'currentDeptNos', 'currentSalaries']);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        //sort for related tables
        $dataProvider->sort->attributes['title'] = [
            // The tables are the ones our relation are configured to
            'asc' => ['titles.title' => SORT_ASC],
            'desc' => ['titles.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dept'] = [
            'asc' => ['departments.dept_name' => SORT_ASC],
            'desc' => ['departments.dept_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query
            ->andFilterWhere([
                'emp_no' => $this->emp_no,
                'birth_date' => $this->birth_date,
                'hire_date' => $this->hire_date,
            ])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'titles.title', $this->title])
            ->andFilterWhere(['like', 'departments.dept_name', $this->dept]);
        return $dataProvider;
    }
}
