<?php

namespace app\domains\aq;

/**
 * This is the ActiveQuery class for [[\app\domains\ar\DepartManager]].
 *
 * @see \app\domains\ar\DepartManager
 */
class DepartManagerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\domains\ar\DepartManager[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\domains\ar\DepartManager|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
