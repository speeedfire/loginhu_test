<?php

namespace app\domains\aq;

/**
 * This is the ActiveQuery class for [[\app\domains\ar\Departments]].
 *
 * @see \app\domains\ar\Departments
 */
class DepartmentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\domains\ar\Departments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\domains\ar\Departments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
